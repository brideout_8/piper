//
//  OpenBetsTableViewController.swift
//  Piper
//
//  Created by Marketing on 4/7/20.
//  Copyright © 2020 Rapdware. All rights reserved.
//

import UIKit
import Firebase

class OpenBetsTableViewController: UITableViewController {
    
    var betsProposedByMe = [Bet]()
    var betsAcceptedByOthers = [Bet]()
    var betsAcceptedByMe = [Bet]()

    @IBOutlet weak var betSegmentControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.backgroundColor = UIColor.systemGroupedBackground
        
        DataService.ds.getUserBets(userId: AppSettings.uid, status: KEY_BET_PROPOSED).getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    guard let bet = Bet(document: document) else {
                        return
                    }
                    self.betsProposedByMe.append(bet)
//                    print("\(document.documentID) => \(document.data())")
                }
                DataService.ds.getUserBets(userId: AppSettings.uid, status: KEY_BET_ACCEPTED).getDocuments() { (querySnapshot, err) in
                    if let err = err {
                        print("Error getting documents: \(err)")
                    } else {
                        for document in querySnapshot!.documents {
                            guard let bet = Bet(document: document) else {
                                return
                            }
                            self.betsAcceptedByOthers.append(bet)
                        }
                    }
                }
                DataService.ds.getUserAcceptedBets(userId: AppSettings.uid).getDocuments() { (querySnapshot, err) in
                    if let err = err {
                        print("Error getting documents: \(err)")
                    } else {
                        for document in querySnapshot!.documents {
                            guard let bet = Bet(document: document) else {
                                return
                            }
                            self.betsAcceptedByMe.append(bet)
                            self.tableView.reloadData()
                        }
                    }
                }
            }
        }
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    @IBAction func segment(_ sender: Any) {
        tableView.reloadData()
    }

    @IBAction func cancelTabbed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        switch(betSegmentControl.selectedSegmentIndex) {
        case 0:
            return betsProposedByMe.count
        case 1:
            return betsAcceptedByOthers.count
        case 2:
            return betsAcceptedByMe.count
        default:
            return 0
        }
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var bet = betsProposedByMe[indexPath.row]
        
        switch betSegmentControl.selectedSegmentIndex {
        case 0:
            bet = betsProposedByMe[indexPath.row]
            break
        case 1:
            bet = betsAcceptedByOthers[indexPath.row]
            break
        case 2:
            bet = betsAcceptedByMe[indexPath.row]
        default:
            bet = betsProposedByMe[indexPath.row]
            break
        }
        if let cell = tableView.dequeueReusableCell(withIdentifier: CELL_USER_BET_CELL) as? UserBetTableViewCell {
            cell.selectionStyle = .none
            cell.configureCell(bet: bet)
            return cell
        } else {
            return UserBetTableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SEGUE_OPEN_BET {
            if let destinationVC = segue.destination as? OpenBetTableViewController {
                let indexPath = tableView.indexPathForSelectedRow!.row
                switch betSegmentControl.selectedSegmentIndex {
                case 0:
                    destinationVC.bet = betsProposedByMe[indexPath]
                case 1:
                    destinationVC.bet = betsAcceptedByOthers[indexPath]
                case 2:
                    destinationVC.bet = betsAcceptedByMe[indexPath]
                default:
                    destinationVC.bet = betsProposedByMe[indexPath]
                }
            }
        }
    }
}
