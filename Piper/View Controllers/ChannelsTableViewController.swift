//
//  ChannelsTableViewController.swift
//  Piper
//
//  Created by Bret Rideout on 2/2/20.
//  Copyright © 2020 Rapdware. All rights reserved.
//

import UIKit
import Firebase

class ChannelsTableViewController: UITableViewController {
    
    private var channels = [Channel]()
    private var channelListener: ListenerRegistration?
    private var currentChannelAlertController: UIAlertController?
    private var memberListner: ListenerRegistration?

//    private let currentUser = User
    
    deinit {
      channelListener?.remove()
        memberListner?.remove()
    }
    
//    init(currentUser: User) {
//      self.currentUser = currentUser
//      super.init(style: .grouped)
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//      fatalError("init(coder:) has not been implemented")
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        AppSettings.uid = nil
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.backgroundColor = UIColor.systemGroupedBackground
        if AppSettings.uid != nil {
            //        UserDefaults.standard.removeObject(forKey: KEY_UID)
        } else {
            do {
                        try Auth.auth().signOut()
                dismiss(animated: true, completion: nil)
//                performSegue(withIdentifier: SEGUE_LOGIN, sender: self)
            //            UserDefaults.standard.set(nil, forKey: KEY_UID)
                    } catch {
                        print("error signing out: \(error.localizedDescription)")
                    }
        }

    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let currentUser = Auth.auth().currentUser {
            navigationItem.largeTitleDisplayMode = .always
            let now = Date()
            let randomNumber = Int(arc4random_uniform(UInt32(10)))
            let date = Calendar.current.date(byAdding: .hour, value: randomNumber, to: now)!
            //Listener for adding or removing channels
            channelListener = DataService.ds.REF_USER_THREADS.addSnapshotListener { querySnapshot, error in
                guard let snapshot = querySnapshot else {
                    print("Error listening for channel updates: \(error?.localizedDescription ?? "No error")")
                    return
                }

                snapshot.documentChanges.forEach { change in
                    self.handleDocumentChange(change)
                }
            }
        } else {
//            performSegue(withIdentifier: SEGUE_LOGIN, sender: self)
            dismiss(animated: true, completion: nil)
        }
    }

    @IBAction func createTabbed(_ sender: Any) {
        
        let ac = UIAlertController(title: "Create a new Channel", message: nil, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        ac.addTextField { field in
          field.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
          field.enablesReturnKeyAutomatically = true
          field.autocapitalizationType = .words
          field.clearButtonMode = .whileEditing
          field.placeholder = "Channel name"
          field.returnKeyType = .done
          field.tintColor = .primary
        }
        
        let createAction = UIAlertAction(title: "Create", style: .default, handler: { _ in
          self.createChannel()
        })
        createAction.isEnabled = false
        ac.addAction(createAction)
        ac.preferredAction = createAction
        
        present(ac, animated: true) {
          ac.textFields?.first?.becomeFirstResponder()
        }
        currentChannelAlertController = ac
    }
    
    @objc private func textFieldDidChange(_ field: UITextField) {
      guard let ac = currentChannelAlertController else {
        return
      }
      
      ac.preferredAction?.isEnabled = field.hasText
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return channels.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let channel = channels[indexPath.row]
        if let cell = tableView.dequeueReusableCell(withIdentifier: CELL_CHANNEL_CELL) as? ChannelCell {
//            cell.selectionStyle = .none
//            if let barImgURL = bar.barImage {
//                if let img = LandBarDetailTableViewController.imageCache.object(forKey: barImgURL) {
//                    cell.configureCell(bar, img: img)
//                    return cell
//                } else {
//                    cell.configureCell(bar)
//                    return cell
//                }
//            } else {
                cell.configureCell(channel: channel)
                return cell
//            }
        } else {
            return ChannelCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: SEGUE_CHAT_VIEW, sender: self)
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */


    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == SEGUE_CHAT_VIEW {
            if let destination = segue.destination as? ChannelChatViewController {
                let indexPath = tableView.indexPathForSelectedRow!.row
                destination.channelName = channels[indexPath].name
                destination.channelId = channels[indexPath].id!
            }
        }
    }
    
    
    @IBAction func userSignedUp(_unwindSegue: UIStoryboardSegue) {
        
    }
    
    @IBAction func userSignedIn(_unwindSegue: UIStoryboardSegue) {
        
    }
    
    // MARK:  - Helpers
    
    private func createChannel() {
      guard let ac = currentChannelAlertController else {
        return
      }

      guard let channelName = ac.textFields?.first?.text else {
        return
      }

      let channel = Channel(name: channelName)
        DataService.ds.REF_CHANNELS.addDocument(data: channel.representation) { (error) in
            if let e = error {
                print("Error saving channel: \(e.localizedDescription)")
            }
        }
    }
    
    private func handleDocumentChange(_ change: DocumentChange) {
//        let data = change.document.data()
//      guard let channel = Channel(document: change.document) else {
//        return
//      }
        
        switch change.type {
        case .added:
            addChannelToTable(change.document.documentID)
          
        case .modified:
          updateChannelInTable(change.document.documentID)
          
        case .removed:
          removeChannelFromTable(change.document.documentID)
        }
    }
    
    private func addChannelToTable(_ channelId: String) {
        DataService.ds.getChannel(channelId: channelId).getDocument { (document, error) in
            if let document = document, document.exists {
                guard let channel = Channel(document: document) else {
                     return
                 }
                 guard !self.channels.contains(channel) else {
                    return
                }
                self.channels.append(channel)
                self.channels.sort()
                
                guard let index = self.channels.firstIndex(of: channel) else {
                    return
                }
                self.tableView.insertRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                self.memberListner = DataService.ds.getChannel(channelId: channel.id!).addSnapshotListener { (document, error) in
                    if let document = document, document.exists {
                        guard let channel = Channel(document: document) else {
                            return
                        }
                        guard let index = self.channels.firstIndex(of: channel) else {
                            return
                        }
                        self.channels[index] = channel
                        self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                    }
                }
            }
        }
    }
    
    private func updateChannelInTable(_ channelId: String) {
        DataService.ds.getChannel(channelId: channelId).getDocument { (document, error) in
            if let document = document, document.exists {
                guard let channel = Channel(document: document) else {
                    return
                }
                 guard let index = self.channels.firstIndex(of: channel) else {
                   return
                 }
                self.channels[index] = channel
                self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
            }
        }
    }
    
    private func removeChannelFromTable(_ channelId: String) {
        DataService.ds.getChannel(channelId: channelId).getDocument { (document, error) in
            if let document = document, document.exists {
                guard let channel = Channel(document: document) else {
                    return
                }
                guard let index = self.channels.firstIndex(of: channel) else {
                    return
                }
            
                self.channels.remove(at: index)
                self.tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
            }
        }
    }
}
