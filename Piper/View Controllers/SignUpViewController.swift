//
//  SignUpViewController.swift
//  Piper
//
//  Created by Bret Rideout on 2/2/20.
//  Copyright © 2020 Rapdware. All rights reserved.
//

import UIKit
import Firebase

class SignUpViewController: UIViewController {

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func signUpTabbed(_ sender: Any) {
//        UserDefaults.standard.set("dada", forKey: KEY_UID)
//        NotificationCenter.default.post(name: .userSignedUp, object: nil)
//        self.dismiss(animated: true, completion: nil)
        if let name = nameField.text, name != "", let email = emailField.text, email != "", let password = passwordField.text, password != "" {
            Auth.auth().createUser(withEmail: email, password: password) { (authData, error) in
                if error != nil {
                    //error
                    showErrorAlert("Error", msg: error.debugDescription, View: self)
                } else {
                    //Log In
//                    NotificationCenter.default.post(name: .userSignedUp, object: nil)
//                    self.dismiss(animated: true, completion: nil)
                    Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
                        let userInfo = ["email":email, "provider":user!.user.providerID, "name":name]
                        DataService.ds.createFirebaseUser(user!.user.uid, user: userInfo)
                        AppSettings.uid = authData!.user.uid
                        AppSettings.displayName = name
                        Analytics.logEvent(AnalyticsEventSignUp, parameters: [
                            AnalyticsParameterMethod:"Email Address"])
//                        UserDefaults.standard.set(authData?.user.uid, forKey: KEY_UID)
                        self.performSegue(withIdentifier: SEGUE_USER_SIGNED_UP, sender: self)
//                        NotificationCenter.default.post(name: .userSignedUp, object: nil)
//                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
        } else {
            //Show error for empty field
        }
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destination.
//        // Pass the selected object to the new view controller.
//        if let destination = segue.destination as? LoginViewController {
//            destination.emailSignUp = emailField.text!
//            destination.passwordSignUp = passwordField.text!
//            destination.nameSignUp = nameField.text!
//        }
//    }
    
    @IBAction func cancelTabbed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
