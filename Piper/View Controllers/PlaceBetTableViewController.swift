//
//  PlaceBetTableViewController.swift
//  Piper
//
//  Created by Bret Rideout on 2/16/20.
//  Copyright © 2020 Rapdware. All rights reserved.
//

import UIKit

class PlaceBetTableViewController: UITableViewController {
    
    @IBOutlet weak var homeTeam: UITextField!
    @IBOutlet weak var awayTeam: UITextField!
    @IBOutlet weak var location: UITextField!
    @IBOutlet weak var overUnder: UITextField!
    @IBOutlet weak var betAmount: UITextField!
    @IBOutlet weak var showDate: UIDatePicker!
    @IBOutlet weak var matchDate: UILabel!
    
    var channelId = String()
    private var currentChannelAlertController: UIAlertController?
    private var showDateVisible = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    @IBAction func cancelTabbed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func showDateAction(_ sender: Any) {
        showDateChanged()
    }
    
    @IBAction func placeBetTabbed(_ sender: Any) {
        guard !homeTeam.text!.isEmpty else {
            showErrorAlert("Error", msg: "Home team is required", View: self)
            return
        }
        guard !awayTeam.text!.isEmpty else {
            showErrorAlert("Error", msg: "Away team is required", View: self)
            return
        }
        guard !overUnder.text!.isEmpty else {
            showErrorAlert("Error", msg: "Over/Under is required", View: self)
            return
        }
        guard !betAmount.text!.isEmpty else {
            showErrorAlert("Error", msg: "Bet amount is required", View: self)
            return
        }

        let ac = UIAlertController(title: "Confirm", message: "Do you want to place this bet?", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//        ac.addAction(UIAlertAction(title: "Confirm", style: .default, handler: nil))

        let createAction = UIAlertAction(title: "Place Bet", style: .default, handler: { _ in
            self.createBet(awayTeam: self.awayTeam.text!, homeTeam: self.homeTeam.text!, overUnder: self.overUnder.text!, location: self.location.text, matchDate: self.matchDate.text!)
        })
        ac.addAction(createAction)
        ac.preferredAction = createAction

        present(ac, animated: true)
        currentChannelAlertController = ac
    }
    
    // MARK: - Helpers
    
    func createBet(awayTeam: String, homeTeam: String, overUnder: String, location: String? = nil, matchDate: String) {
        let bet = Bet(price: betAmount.text!, channelId: channelId, awayTeam: awayTeam, homeTeam: homeTeam, overUnder: overUnder, location: location, matchDate: matchDate)
        let betSaved = DataService.ds.createBet(bet: bet)
        let message = Message(bet: bet, betId: betSaved)
        let messageArray = DataService.ds.saveMessage(message: message, channelId: channelId)
        DataService.ds.updateBet(betId: betSaved, channelId: channelId, messageId: messageArray["MessageId"] as! String, status: KEY_BET_PROPOSED)
        dismiss(animated: true, completion: nil)
    }
    
    private func toggleShowDateDatepicker () {
         showDateVisible = !showDateVisible
     
         tableView.beginUpdates()
         tableView.endUpdates()
    }
    
    private func showDateChanged () {
        matchDate.text = DateFormatter.localizedString(from: showDate.date, dateStyle: DateFormatter.Style.long, timeStyle: DateFormatter.Style.short)
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 8
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 5 {
                 toggleShowDateDatepicker()
            }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if !showDateVisible && indexPath.row == 6 {
             return 0
        } else {
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
