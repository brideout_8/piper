//
//  ViewController.swift
//  Piper
//
//  Created by Bret Rideout on 2/2/20.
//  Copyright © 2020 Rapdware. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        UserDefaults.standard.removeObject(forKey: KEY_UID)
//        NotificationCenter.default.addObserver(
//          self,
//          selector: #selector(userSignUp),
//          name: .userSignedUp,
//          object: nil
//        )
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if AppSettings.uid != nil {
            performSegue(withIdentifier: SEGUE_CHANNELS, sender: self)
        }
    }
    
//    @objc private func userSignUp() {
////        if UserDefaults.standard.value(forKey: KEY_UID) != nil {
//            print("here")
//            print(emailSignUp)
//            Auth.auth().signIn(withEmail: emailSignUp, password: passwordSignUp) { (authData, error) in
//                if error != nil {
//                    //Show Error
//                    showErrorAlert("Error", msg: "No account exists", View: self)
//                } else {
//                    //Perform Segue
//                    UserDefaults.standard.set(authData?.user.uid, forKey: KEY_UID)
//                    let userInfo = ["email":self.emailSignUp, "provider":authData!.user.providerID, "name":self.nameSignUp, "userId":authData!.user.uid]
//                    DataService.ds.createFirebaseUser(authData!.user.uid, user: userInfo)
//                    self.performSegue(withIdentifier: SEGUE_CHANNELS, sender: self)
//                }
//            }
////        }
//    }

    @IBAction func signIn(_ sender: Any) {
        if let emailAdd = email.text, emailAdd != "", let pwd = password.text, pwd != "" {
            Auth.auth().signIn(withEmail: emailAdd, password: pwd) { (authData, error) in
                if error != nil {
                    //Show Error
                    showErrorAlert("Error", msg: "No account exists", View: self)
                } else {
                    //Perform Segue
//                    UserDefaults.standard.set(authData?.user.uid, forKey: KEY_UID)
                    //Get User Name
                    AppSettings.uid = authData!.user.uid
                    DataService.ds.REF_USER_CURRENT.getDocument { (document, error) in
                        if let document = document, document.exists {
//                            let dataDescription = document.data().map(String.init(describing:)) ?? "nil"
//                            print("Document data: \(dataDescription)")
                            AppSettings.displayName = document.get("name") as? String
                            AppSettings.channels = document.get("channels") as? Array
                            Analytics.logEvent(AnalyticsEventLogin, parameters: nil)
//                            self.performSegue(withIdentifier: SEGUE_USER_SIGNED_IN, sender: self)
                            self.performSegue(withIdentifier: SEGUE_CHANNELS, sender: self)
                        } else {
                            showErrorAlert("Error", msg: "Plase contact support with error: UD1001", View: self)
                        }
                    }
                }
            }
        }
    }
    @IBAction func signUpTabbed(_ sender: Any) {
        self.performSegue(withIdentifier: SEGUE_SIGNUP, sender: self)
    }
}

