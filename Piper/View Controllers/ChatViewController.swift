/*
MIT License

Copyright (c) 2017-2019 MessageKit

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

import UIKit
import MessageKit
import InputBarAccessoryView
import Firebase

/// A base class for the example controllers
class ChatViewController: MessagesViewController, MessagesDataSource {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    /// The `BasicAudioController` controll the AVAudioPlayer state (play, pause, stop) and udpate audio cell UI accordingly.
//    open lazy var audioController = BasicAudioController(messageCollectionView: messagesCollectionView)

    var messageList: [Message] = []
    var channelName = String()
    var channelId = String()
    private var messageListener: ListenerRegistration?
    private var firstLoad = true
    
    let refreshControl = UIRefreshControl()
    
    let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureMessageCollectionView()
        configureMessageInputBar()
//        loadFirstMessages()
        title = channelName
        
//        let message = Message(content: "tst")
//        DataService.ds.REF_THREAD(channelId: channelId).addDocument(data: message.representation) { error in
//          if let e = error {
//            print("Error sending message: \(e.localizedDescription)")
//            return
//          }
//
////          self.messagesCollectionView.scrollToBottom()
//        }
        
        messageListener = DataService.ds.getMessages(channelId: channelId).addSnapshotListener { querySnapshot, error in
          guard let snapshot = querySnapshot else {
            print("Error listening for channel updates: \(error?.localizedDescription ?? "No error")")
            return
          }
          
          snapshot.documentChanges.forEach { change in
            self.handleDocumentChange(change, firstLoad: self.firstLoad)
          }
            self.firstLoad = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        MockSocket.shared.connect(with: [SampleData.shared.nathan, SampleData.shared.wu])
//            .onNewMessage { [weak self] message in
//                self?.insertMessage(message)
//        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        MockSocket.shared.disconnect()
//        audioController.stopAnyOngoingPlaying()
    }
    
//    func loadFirstMessages() {
//        DispatchQueue.global(qos: .userInitiated).async {
//            let count = UserDefaults.standard.mockMessagesCount()
//            SampleData.shared.getMessages(count: count) { messages in
//                DispatchQueue.main.async {
//                    self.messageList = messages
//                    self.messagesCollectionView.reloadData()
//                    self.messagesCollectionView.scrollToBottom()
//                }
//            }
//        }
//    }
    
    deinit {
      messageListener?.remove()
    }
    
    @objc
    func loadMoreMessages() {
        DataService.ds.getMoreMessages(channelId: channelId, dateSent: messageList.first!.sentDate).getDocuments { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for messageDocument in querySnapshot!.documents {
//                    print("\(document.documentID) => \(document.data())")
                    let data = messageDocument.data()
                    if data["bet"] as! Bool == true {
                        if let betId = data["betId"] as? String {
                            //This is a bet
                            DataService.ds.getBet(betId: betId).getDocument { (document, error) in
                                if let document = document, document.exists {
                                    guard let betData = Bet(document: document) else {
                                        print("ERROR")
                                        return
                                    }
                                    let message = Message(document: messageDocument, bet: betData)
                                    self.loadNewMessages(message: message)
                                } else {
                                    let message = Message(document: messageDocument)
                                    self.loadNewMessages(message: message)
                                }
                            }
                        } else {
                            let message = Message(document: messageDocument)
                            self.loadNewMessages(message: message)
                        }
                    } else {
                        let message = Message(document: messageDocument)
                        self.loadNewMessages(message: message)
                    }
//                    let message = Message(document: document)
//                    guard !self.messageList.contains(message) else {
//                        self.refreshControl.endRefreshing()
//                      return
//                    }
//                    self.messageList.insert(message, at: 0)
//                    self.messageList.sort()
//                    self.messagesCollectionView.reloadDataAndKeepOffset()
                }
            }
        }
        refreshControl.endRefreshing()
    }
    
    func loadNewMessages(message: Message) {
        guard !self.messageList.contains(message) else {
            self.refreshControl.endRefreshing()
            return
        }
        self.messageList.insert(message, at: 0)
        self.messageList.sort()
        self.messagesCollectionView.reloadDataAndKeepOffset()
    }
    
    func configureMessageCollectionView() {
        
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messageCellDelegate = self
        
        scrollsToBottomOnKeyboardBeginsEditing = true // default false
        maintainPositionOnKeyboardFrameChanged = true // default false
        
        messagesCollectionView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(loadMoreMessages), for: .valueChanged)
    }
    
    func configureMessageInputBar() {
        messageInputBar.delegate = self
        messageInputBar.inputTextView.tintColor = .primary
        messageInputBar.sendButton.setTitleColor(.primary, for: .normal)
        messageInputBar.inputTextView.placeholder = "Write your message..."
        messageInputBar.sendButton.setTitleColor(
            UIColor.primary.withAlphaComponent(0.3),
            for: .highlighted
        )
    }
    
    // MARK: - Helpers
    
    private func handleDocumentChange(_ change: DocumentChange, firstLoad: Bool) {
        let data = change.document.data()
        switch change.type {
        case .added:
            if data["bet"] as! Bool == true {
                if let betId = data["betId"] as? String {
                    //This is a bet
                    DataService.ds.getBet(betId: betId).getDocument { (document, error) in
                        if let document = document, document.exists {
                            guard let betData = Bet(document: document) else {
                                print("ERROR")
                                return
                            }
                            let message = Message(document: change.document, bet: betData)
                            self.insertNewMessage(message, firstLoad: firstLoad)
                        } else {
                            let message = Message(document: change.document)
                            self.insertNewMessage(message, firstLoad: firstLoad)
                        }
                    }
                } else {
                    let message = Message(document: change.document)
                    insertNewMessage(message, firstLoad: firstLoad)
                }
            } else {
                let message = Message(document: change.document)
                insertNewMessage(message, firstLoad: firstLoad)
            }
        case .modified:
            print("!!Modified")
            if let betId = data["betId"] as? String {
                updateBetMessage(change: change, betId: betId)
            }
        default:
            break
        }
//        let message = Message(document: change.document)
//        insertNewMessage(message, firstLoad: firstLoad)
//      switch change.type {
//      case .added:
//        insertNewMessage(message, firstLoad: firstLoad)
//      default:
//        break
//      }
    }
    
    private func insertNewMessage(_ message: Message, firstLoad: Bool) {
        messageList.append(message)
        messageList.sort()
        let isLatestMessage = messageList.firstIndex(of: message) == (messageList.count - 1)
        _ = messagesCollectionView.isAtBottom && isLatestMessage
        if !firstLoad {
//        messagesCollectionView.performBatchUpdates(nil, completion: nil)
            // Reload last section to update header/footer labels and insert a new one
            messagesCollectionView.performBatchUpdates({
                messagesCollectionView.insertSections([messageList.count - 1])
                if messageList.count >= 2 {
                    messagesCollectionView.reloadSections([messageList.count - 2])
                }
            }, completion: { [weak self] _ in
                if self?.isLastSectionVisible() == true {
                    self?.messagesCollectionView.scrollToBottom(animated: true)
                }
            })
        } else {
            messagesCollectionView.reloadData()
        }
        
    }
    
    private func updateBetMessage(change: DocumentChange, betId: String) {
        DataService.ds.getBet(betId: betId).getDocument { (document, error) in
            if let document = document, document.exists {
                guard let betData = Bet(document: document) else {
                    return
                }
                let message = Message(document: change.document, bet: betData)
                guard let index = self.messageList.firstIndex(of: message) else {
                    return
                }
                self.messageList[index] = message
                self.messagesCollectionView.reloadData()
            }
        }
    }
    
    func isLastSectionVisible() -> Bool {
        
        guard !messageList.isEmpty else { return false }
        
        let lastIndexPath = IndexPath(item: 0, section: messageList.count - 1)
        
        return messagesCollectionView.indexPathsForVisibleItems.contains(lastIndexPath)
    }
    
    // MARK: - MessagesDataSource
    
    func currentSender() -> SenderType {
        return Sender(id: AppSettings.uid, displayName: AppSettings.displayName)
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messageList.count
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return messageList[indexPath.section]
    }
    
    func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        if indexPath.section % 3 == 0 {
            return NSAttributedString(string: MessageKitDateFormatter.shared.string(from: message.sentDate), attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        }
        return nil
    }
    
//    func cellBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
//
//        return NSAttributedString(string: "Read", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor.darkGray])
//    }
    
    func messageTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        let name = message.sender.displayName
        return NSAttributedString(string: name, attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption1)])
    }
    
    func messageBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        
        let dateString = formatter.string(from: message.sentDate)
        return NSAttributedString(string: dateString, attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption2)])
    }
    
}

// MARK: - MessageCellDelegate

extension ChatViewController: MessageCellDelegate {
    
    func didTapAvatar(in cell: MessageCollectionViewCell) {
        print("Avatar tapped")
    }
    
    func didTapMessage(in cell: MessageCollectionViewCell) {
        print("Message tapped")
    }
    
    func didTapCellTopLabel(in cell: MessageCollectionViewCell) {
        print("Top cell label tapped")
    }
    
    func didTapCellBottomLabel(in cell: MessageCollectionViewCell) {
        print("Bottom cell label tapped")
    }
    
    func didTapMessageTopLabel(in cell: MessageCollectionViewCell) {
        print("Top message label tapped")
    }
    
    func didTapMessageBottomLabel(in cell: MessageCollectionViewCell) {
        print("Bottom label tapped")
    }

    func didTapPlayButton(in cell: AudioMessageCell) {
        guard let indexPath = messagesCollectionView.indexPath(for: cell),
            let message = messagesCollectionView.messagesDataSource?.messageForItem(at: indexPath, in: messagesCollectionView) else {
                print("Failed to identify message when audio cell receive tap gesture")
                return
        }
//        guard audioController.state != .stopped else {
//            // There is no audio sound playing - prepare to start playing for given audio message
//            audioController.playSound(for: message, in: cell)
//            return
//        }
//        if audioController.playingMessage?.messageId == message.messageId {
//            // tap occur in the current cell that is playing audio sound
//            if audioController.state == .playing {
//                audioController.pauseSound(for: message, in: cell)
//            } else {
//                audioController.resumeSound()
//            }
//        } else {
//            // tap occur in a difference cell that the one is currently playing sound. First stop currently playing and start the sound for given message
//            audioController.stopAnyOngoingPlaying()
//            audioController.playSound(for: message, in: cell)
//        }
    }

    func didStartAudio(in cell: AudioMessageCell) {
        print("Did start playing audio sound")
    }

    func didPauseAudio(in cell: AudioMessageCell) {
        print("Did pause audio sound")
    }

    func didStopAudio(in cell: AudioMessageCell) {
        print("Did stop audio sound")
    }

    func didTapAccessoryView(in cell: MessageCollectionViewCell) {
        print("Accessory view tapped")
    }

}

// MARK: - MessageLabelDelegate

extension ChatViewController: MessageLabelDelegate {
    
    func didSelectAddress(_ addressComponents: [String: String]) {
        print("Address Selected: \(addressComponents)")
    }
    
    func didSelectDate(_ date: Date) {
        print("Date Selected: \(date)")
    }
    
    func didSelectPhoneNumber(_ phoneNumber: String) {
        print("Phone Number Selected: \(phoneNumber)")
    }
    
    func didSelectURL(_ url: URL) {
        print("URL Selected: \(url)")
    }
    
    func didSelectTransitInformation(_ transitInformation: [String: String]) {
        print("TransitInformation Selected: \(transitInformation)")
    }

    func didSelectHashtag(_ hashtag: String) {
        print("Hashtag selected: \(hashtag)")
    }

    func didSelectMention(_ mention: String) {
        print("Mention selected: \(mention)")
    }

    func didSelectCustom(_ pattern: String, match: String?) {
        print("Custom data detector patter selected: \(pattern)")
    }

}

// MARK: - MessageInputBarDelegate

extension ChatViewController: InputBarAccessoryViewDelegate {

    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {

        // Here we can parse for which substrings were autocompleted
        let attributedText = messageInputBar.inputTextView.attributedText!
        let range = NSRange(location: 0, length: attributedText.length)
        attributedText.enumerateAttribute(.autocompleted, in: range, options: []) { (_, range, _) in

            let substring = attributedText.attributedSubstring(from: range)
            let context = substring.attribute(.autocompletedContext, at: 0, effectiveRange: nil)
            print("Autocompleted: `", substring, "` with context: ", context ?? [])
        }

//        let components = inputBar.inputTextView.components
        messageInputBar.inputTextView.text = String()
        messageInputBar.invalidatePlugins()

        // Send button activity animation
        messageInputBar.sendButton.startAnimating()
        messageInputBar.inputTextView.placeholder = "Sending..."
        //Save to firebase
        let message = Message(content: text)
        let messageRecievedDict = DataService.ds.saveMessage(message: message, channelId: channelId)
        let messageRecieved = messageRecievedDict["Message Sent"] as! Bool
        if messageRecieved == true {
            //update the sent message to delieverd message
            self.messageInputBar.sendButton.stopAnimating()
            self.messageInputBar.inputTextView.placeholder = "Write Your Message"
            self.messagesCollectionView.scrollToBottom(animated: true)
        } else {
            self.messageInputBar.sendButton.stopAnimating()
            self.messageInputBar.inputTextView.placeholder = "Write Your Message"
            self.messagesCollectionView.scrollToBottom(animated: true)
        }
//        DispatchQueue.global(qos: .default).async {
//            // fake send request task
//            sleep(1)
//            DispatchQueue.main.async { [weak self] in
//                self?.messageInputBar.sendButton.stopAnimating()
//                self?.messageInputBar.inputTextView.placeholder = "Aa"
//                self?.insertMessages(components)
//                self?.messagesCollectionView.scrollToBottom(animated: true)
//            }
//        }
    }

//    private func insertMessages(_ data: [Any]) {
//        for component in data {
//            let user = SampleData.shared.currentSender
//            if let str = component as? String {
//                let message = MockMessage(text: str, user: user, messageId: UUID().uuidString, date: Date())
//                insertMessage(message)
//            } else if let img = component as? UIImage {
//                let message = MockMessage(image: img, user: user, messageId: UUID().uuidString, date: Date())
//                insertMessage(message)
//            }
//        }
//    }
}
