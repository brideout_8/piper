//
//  OpenBetTableViewController.swift
//  Piper
//
//  Created by Marketing on 4/19/20.
//  Copyright © 2020 Rapdware. All rights reserved.
//

import UIKit

class OpenBetTableViewController: UITableViewController {

    @IBOutlet weak var homeTeamLabel: UILabel!
    @IBOutlet weak var awayTeamLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var overUnderLabel: UILabel!
    @IBOutlet weak var betLabel: UILabel!
    @IBOutlet weak var groupLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var matchDate: UILabel!
    @IBOutlet weak var proposedByLabel: UILabel!
    @IBOutlet weak var acceptedByLabel: UILabel!
    
    private var currentChannelAlertController: UIAlertController?
    var bet: Bet!
    var userProposed = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.backgroundColor = UIColor.systemGroupedBackground
        
        homeTeamLabel.text = bet.homeTeam
        awayTeamLabel.text = bet.awayTeam
        locationLabel.text = bet.location
        overUnderLabel.text = bet.overUnder
        betLabel.text = "$\(bet.betAmount)"
        dateLabel.text = bet.sentDate.toString(dateFormat: "MM/dd/YYYY")
        matchDate.text = bet.matchDate
        proposedByLabel.text = bet.senderUserName
        acceptedByLabel.text = bet.acceptedUserName
        let channelRef = DataService.ds.getChannel(channelId: bet.channelId)
        channelRef.getDocument{(document, error) in
            if let document = document, document.exists {
                let data = document.data()
                self.groupLabel.text = data!["name"] as? String
            }
        }
        
        if AppSettings.uid == bet.senderUserId {
            userProposed = true
        }

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    @IBAction func removeBetTabbed(_ sender: Any) {
        let ac = UIAlertController(title: "Confirm", message: "Do you want to withdraw this bet?", preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        //        ac.addAction(UIAlertAction(title: "Confirm", style: .default, handler: nil))

        let createAction = UIAlertAction(title: "Withdraw Bet", style: .destructive, handler: { _ in
                self.removeBet()
            })
            ac.addAction(createAction)
            ac.preferredAction = createAction

            present(ac, animated: true)
            currentChannelAlertController = ac
    }
    
    func removeBet() {
        DataService.ds.updateBet(betId: bet.id, channelId: bet.channelId, messageId: bet.messageId!, status: KEY_BET_WITHDRAWN)
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if bet.status == KEY_BET_PROPOSED {
            return 11
        } else if bet.status == KEY_BET_ACCEPTED && userProposed {
            return 10
        } else {
            return 10
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if bet.status == KEY_BET_PROPOSED{
            if indexPath.row == 7 {
                return 0
            } else {
                return super.tableView(tableView, heightForRowAt: indexPath)
            }
        } else {
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
