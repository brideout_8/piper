//
//  Constants.swift
//  Piper
//
//  Created by Bret Rideout on 2/2/20.
//  Copyright © 2020 Rapdware. All rights reserved.
//

import Foundation
import UIKit

// MARK: - Segues
let SEGUE_LOGIN = "logInSegue"
let SEGUE_CHANNELS = "channelsSegue"
let SEGUE_SIGNUP = "signUpSegue"
let SEGUE_USER_SIGNED_UP = "userSignedUpSegue"
let SEGUE_CHAT_VIEW = "chatViewSegue"
let SEGUE_USER_SIGNED_IN = "userSignedIn"
let SEGUE_CHAT_INFO = "chatInfoSegue"
let SEGUE_PLACE_BET = "placeBetSegue"
let SEGUE_OPEN_BET = "openBetSegue"

// MARK: - Keys
let KEY_UID = "uid"
let KEY_DISPLAY_NAME = "displayName"
let KEY_BET_PROPOSED = "proposed"
let KEY_BET_ACCEPTED = "accepted"
let KEY_BET_WITHDRAWN = "withdrawn"
let KEY_BET_REJECTED = "rejected"
let KEY_BET_MESSAGE = "message"

// MARK: - Cells
let CELL_CHANNEL_CELL = "channelCell"
let CELL_USER_BET_CELL = "userBetCell"

// MARK: - Material
let SHADOW_COLOR: CGFloat = 157.0 / 255.0
let BORDER_COLOR: CGFloat = 110.0 / 255.0

// MARK: - Functions
func showErrorAlert(_ title: String, msg: String, View: UIViewController) {
    let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
    let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
    alert.addAction(action)
    View.present(alert, animated: true, completion: nil)
}

extension Date
{
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }

}
