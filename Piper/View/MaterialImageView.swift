//
//  MaterialImageView.swift
//  Piper
//
//  Created by Bret Rideout on 2/16/20.
//  Copyright © 2020 Rapdware. All rights reserved.
//

import UIKit

class MaterialImageView: UIImageView {

    override func awakeFromNib() {
        // Drawing code
//        layer.cornerRadius = 5.0
        layer.masksToBounds = true
        layer.cornerRadius = bounds.width / 2
        layer.borderColor = UIColor.primary.cgColor
        layer.borderWidth = 6
//        layer.shadowColor = UIColor(red: SHADOW_COLOR, green: SHADOW_COLOR, blue: SHADOW_COLOR, alpha: 0.5).cgColor?
//        layer.shadowOpacity = 0.8
//        layer.shadowRadius = 5.0
//        layer.shadowOffset = CGSize(width: 0.0, height: 1.5)
    }

}
