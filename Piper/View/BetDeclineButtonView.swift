//
//  BetDeclineButtonView.swift
//  Piper
//
//  Created by Bret Rideout on 2/28/20.
//  Copyright © 2020 Rapdware. All rights reserved.
//

import UIKit

class BetDeclineButtonView: UIView {

    override func awakeFromNib() {
        // Drawing code
        layer.cornerRadius = 15.0
        layer.maskedCorners = [.layerMaxXMaxYCorner]
        layer.borderWidth = 1
        layer.borderColor = UIColor.systemGray.cgColor
    }
}
