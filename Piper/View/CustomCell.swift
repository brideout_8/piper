/*
 MIT License
 
 Copyright (c) 2017-2019 MessageKit
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import UIKit
import MessageKit

open class CustomCell: UICollectionViewCell {
    
   
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var oddsLabel: UILabel!
    @IBOutlet weak var betAmount: UILabel!
    @IBOutlet var cellView: UIView!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var proposedLabel: UILabel!
    @IBOutlet weak var rejectButton: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    var proposer = ""
    var sameUser = false
    var acceptedBy = ""
    var betId: String?
    var messageId: String!
    var channelId: String!
//    private var currentChannelAlertController: UIAlertController?
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupSubviews()
    }
    
    @IBAction func acceptTabbed(_ sender: Any) {
        let ac = UIAlertController(title: "Confirm", message: "Do you want to accept this bet?", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//        ac.addAction(UIAlertAction(title: "Confirm", style: .default, handler: nil))

        let createAction = UIAlertAction(title: "Accept Bet", style: .default, handler: { _ in
            self.acceptBet()
        })
        ac.addAction(createAction)
        ac.preferredAction = createAction
        parentViewController?.present(ac, animated: true, completion: nil)
    }
    
    @IBAction func rejectTabbed(_ sender: Any) {
        guard let betId = betId else {
            return
        }
        if sameUser {
            //Remove Bet
            DataService.ds.updateBet(betId: betId, channelId: channelId, messageId: messageId, status: KEY_BET_WITHDRAWN)
        } else {
            //Reject Bet
            DataService.ds.updateBet(betId: betId, channelId: channelId, messageId: messageId, status: KEY_BET_REJECTED)
        }
    }
    
    func acceptBet() {
        guard let betId = betId else {
            return
        }
         DataService.ds.updateBet(betId: betId, channelId: channelId, messageId: messageId, status: KEY_BET_ACCEPTED)
    }
    
    open func setupSubviews() {
        Bundle.main.loadNibNamed("CustomCell", owner: self, options: nil)
        contentView.addSubview(cellView)
        label.textColor = UIColor(red: BORDER_COLOR, green: BORDER_COLOR, blue: BORDER_COLOR, alpha: 1.0)
        oddsLabel.textColor = UIColor(red: BORDER_COLOR, green: BORDER_COLOR, blue: BORDER_COLOR, alpha: 1.0)
        betAmount.textColor = UIColor(red: BORDER_COLOR, green: BORDER_COLOR, blue: BORDER_COLOR, alpha: 1.0)
        proposedLabel.textColor = UIColor(red: BORDER_COLOR, green: BORDER_COLOR, blue: BORDER_COLOR, alpha: 1.0)
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        cellView.frame = contentView.bounds
    }
    
    func configure(with message: Message, isTimeLabelVisible: Bool, at indexPath: IndexPath, and messagesCollectionView: MessagesCollectionView) {
        self.betId = message.betId
        self.messageId = message.id
        self.channelId = message.channelId
        // Do stuff
        switch message.kind {
        case .custom( _):
//            guard let systemMessage = data as? String else { return }
            label.text = "\(message.awayTeam) @ \(message.homeTeam)"
            oddsLabel.text = "\(message.homeTeam) (\(message.overUnder))"
            betAmount.text = "$\(message.betAmount)"
            rejectButton.setTitle("Reject", for: .normal)
            rejectButton.setTitleColor(.red, for: .normal)
            rejectButton.isEnabled = true
            acceptButton.isEnabled = true
            acceptButton.setTitleColor(.systemGray2, for: .normal)
            acceptButton.setTitle("Accept", for: .normal)
            switch message.status {
            case KEY_BET_WITHDRAWN:
                acceptButton.isEnabled = false
                rejectButton.isEnabled = false
                acceptButton.setTitleColor(.systemGray2, for: .normal)
                rejectButton.setTitleColor(.red, for: .normal)
                rejectButton.setTitle("Withdrawn", for: .normal)
                proposedLabel.text = "Withdrawn by \(message.sender.displayName)"
                return
            case KEY_BET_ACCEPTED:
                acceptButton.isEnabled = false
                rejectButton.isEnabled = false
                acceptButton.setTitle("Accpeted", for: .normal)
                rejectButton.setTitleColor(.systemGray2, for: .normal)
                proposedLabel.text = "Proposed by \(message.sender.displayName) - Accepted by \(message.acceptedUserName)"
                if message.acceptedUserId == AppSettings.uid {
                    acceptButton.setTitleColor(UIColor(red: 0/255, green: 125/255, blue: 0/255, alpha: 1.0), for: .normal)
                }
            case KEY_BET_PROPOSED:
                proposedLabel.text = "Proposed by \(message.sender.displayName)"
                acceptButton.isEnabled = true
                rejectButton.isEnabled = true
                acceptButton.setTitle("Accept", for: .normal)
                rejectButton.setTitle("Reject", for: .normal)
            default:
                return
            }
            if message.sender.senderId == AppSettings.uid {
                acceptButton.isEnabled = false
                acceptButton.setTitleColor(UIColor(red: 0/255, green: 125/255, blue: 0/255, alpha: 1.0), for: .normal)
                acceptButton.setTitle("Your Bet", for: .normal)
                sameUser = true
            }
//            if isTimeLabelVisible {
                dateLabel.attributedText = NSAttributedString(string: MessageKitDateFormatter.shared.string(from: message.sentDate), attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor.darkGray])
//            } else {
//                dateLabel.isHidden = true
//            }
        default:
            break
        }
        if let rejectedUsers = message.rejectedUsers {
            if rejectedUsers.contains(AppSettings.uid) {
                acceptButton.isEnabled = false
                rejectButton.isEnabled = false
                rejectButton.setTitle("Rejected", for: .normal)
                rejectButton.setTitleColor(.red, for: .normal)
            }
        }
    }
    
}
extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if parentResponder is UIViewController {
                return parentResponder as? UIViewController
            }
        }
        return nil
    }
}
