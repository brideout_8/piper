//
//  ChannelCell.swift
//  Piper
//
//  Created by Bret Rideout on 2/5/20.
//  Copyright © 2020 Rapdware. All rights reserved.
//

import UIKit

class ChannelCell: UITableViewCell {

    @IBOutlet weak var channelLabel: UILabel!
    @IBOutlet weak var memberCountLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(channel: Channel) {
        self.channelLabel.text = channel.name
        self.memberCountLabel.text = String(channel.memberCount) + " Members"
        self.channelLabel.textColor = UIColor(red: BORDER_COLOR, green: BORDER_COLOR, blue: BORDER_COLOR, alpha: 1.0)
        self.memberCountLabel.textColor = UIColor(red: BORDER_COLOR, green: BORDER_COLOR, blue: BORDER_COLOR, alpha: 1.0)
    }
}
