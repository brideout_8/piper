//
//  BetView.swift
//  Piper
//
//  Created by Bret Rideout on 2/27/20.
//  Copyright © 2020 Rapdware. All rights reserved.
//

import UIKit

class BetView: UIView {

    override func awakeFromNib() {
        // Drawing code
        layer.cornerRadius = 15.0
        layer.borderWidth = 1
        layer.borderColor = UIColor(red: BORDER_COLOR, green: BORDER_COLOR, blue: BORDER_COLOR, alpha: 1.0).cgColor
//        layer.shadowColor = UIColor(red: SHADOW_COLOR, green: SHADOW_COLOR, blue: SHADOW_COLOR, alpha: 0.5).cgColor
//        layer.shadowOpacity = 0.8
//        layer.shadowRadius = 15.0
//        layer.shadowOffset = CGSize(width: 0.0, height: 1.5)
    }
}
