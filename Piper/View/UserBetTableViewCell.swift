//
//  userBetTableViewCell.swift
//  Piper
//
//  Created by Marketing on 4/7/20.
//  Copyright © 2020 Rapdware. All rights reserved.
//

import UIKit
import Firebase

class UserBetTableViewCell: UITableViewCell {
    
    @IBOutlet weak var teamsLabel: UILabel!
    @IBOutlet weak var oddsLabel: UILabel!
    @IBOutlet weak var betLabel: UILabel!
    @IBOutlet weak var matchDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(bet: Bet) {
        teamsLabel.text = "\(bet.awayTeam) @ \(bet.homeTeam)"
        oddsLabel.text = bet.overUnder
        betLabel.text = "$\(bet.betAmount)"
        matchDate.text = bet.matchDate
    }

}
