//
//  User.swift
//  Piper
//
//  Created by Bret Rideout on 3/1/20.
//  Copyright © 2020 Rapdware. All rights reserved.
//

import Foundation
import FirebaseFirestore

struct User {
  
    let id: String?
    let name: String?
    let email: String?
    let provider: String?

    init(name: String) {
        id = nil
        self.name = name
        email = nil
        provider = nil
    }

    init?(document: DocumentSnapshot) {
        let data = document.data()

        guard let name = data!["name"] as? String else {
            return nil
        }
        self.name = name
        id = document.documentID
        if let provider = data!["provider"] as? String {
            self.provider = provider
        } else {
            self.provider = ""
        }
        if let email = data!["email"] as? String {
            self.email = email
        } else {
            self.email = ""
        }
    }
}

extension User: DatabaseRepresentation {
  
  var representation: [String : Any] {
    var rep = ["name": name]
    
    if let id = id {
      rep["id"] = id
    }
    if let email = email {
        rep["email"] = email
    }
    if let provider = provider {
        rep["provider"] = provider
    }
    if let name = name {
        rep["name"] = name
    }
    
    return rep as [String : Any]
  }
  
}
