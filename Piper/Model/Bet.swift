//
//  Bet.swift
//  Piper
//
//  Created by Bret Rideout on 2/16/20.
//  Copyright © 2020 Rapdware. All rights reserved.
//

import Foundation
import UIKit
import MessageKit
import Firebase

class Bet {
    fileprivate var _id: String!
    fileprivate var _sender: SenderType!
    fileprivate var _sentDate: Date!
    fileprivate var _matchDate: String!
    fileprivate var _betAmount: String!
    fileprivate var _betAccepted: Bool!
    fileprivate var _acceptedUserId: String!
    fileprivate var _acceptedUserName: String!
    fileprivate var _senderUserName: String!
    fileprivate var _senderUserId: String!
    fileprivate var _channelId: String!
    fileprivate var _status: String!
    fileprivate var _awayTeam: String!
    fileprivate var _homeTeam: String!
    fileprivate var _overUnder: String!
    fileprivate var _location: String!
    fileprivate var _rejectedUsers: [String]?
    fileprivate var _messageId: String?
    
    var id: String {
        return _id
    }
    
    var sender: SenderType {
        return _sender
    }
    
    var sentDate: Date {
        return _sentDate
    }
    
    var betAmount: String {
        return _betAmount
    }
    
    var matchDate: String {
        return _matchDate
    }
    
    var betAccepted: Bool {
        return _betAccepted
    }
    
    var accectpedUserId: String {
        return _acceptedUserId
    }
    
    var acceptedUserName: String {
        return _acceptedUserName
    }
    
    var senderUserName: String {
        return _senderUserName
    }
    
    var senderUserId: String {
        return _senderUserId
    }
    
    var channelId: String {
        return _channelId
    }
    
    var status: String {
        return _status
    }
    
    var awayTeam: String {
        return _awayTeam
    }
    
    var homeTeam: String {
        return _homeTeam
    }
    
    var overUnder: String {
        return _overUnder
    }
    
    var location: String {
        return _location
    }
    
    var rejectedUsers: [String]? {
        return _rejectedUsers
    }
    
    var messageId: String? {
        return _messageId
    }
    
    init(price: String, channelId: String, awayTeam: String, homeTeam: String, overUnder: String, location: String? = nil, matchDate: String) {
        self._id = nil
        self._sender = Sender(id: AppSettings.uid, displayName: AppSettings.displayName)
        self._sentDate = Date()
        self._matchDate = matchDate
        self._betAmount = price
        self._betAccepted = false
        self._senderUserId = AppSettings.uid
        self._senderUserName = AppSettings.displayName
        self._channelId = channelId
        self._status = KEY_BET_PROPOSED
        self._awayTeam = awayTeam
        self._homeTeam = homeTeam
        self._overUnder = overUnder
        self._location = location
    }
    
    init?(document: DocumentSnapshot) {
        let data = document.data()!
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
        
        guard let senderId = data["senderUserId"] as? String else {
            return nil
        }
        guard let senderName = data["senderUserName"] as? String else {
            return nil
        }
        guard let timeStamp = data["created"] as? Timestamp else {
            return nil
        }
        guard let statusServer = data["status"] as? String else {
            return nil
        }
        let dateInt = timeStamp.dateValue()
        let dateString = dateFormatter.string(from: dateInt)
        guard let sentDate = dateFormatter.date(from: dateString) else {
            return nil
        }

        if let usersRejected = data["rejectedUsers"] as? [String:AnyObject] {
            var users = [String]()
            for (key, _) in usersRejected {
                users.append(key)
            }
            self._rejectedUsers = users
        }
        
        self._id = document.documentID
        self._sender = Sender(id: senderId, displayName: senderName)
        self._sentDate = sentDate
        self._betAmount = data["betAmount"] as? String
        self._betAccepted = data["accepted"] as? Bool
        self._acceptedUserId = data["acceptedUserId"] as? String
        self._acceptedUserName = data["acceptedUserName"] as? String
        self._senderUserId = senderId
        self._senderUserName = senderName
        self._channelId = data["channelId"] as? String
        self._status = statusServer
        self._awayTeam = data["awayTeam"] as? String
        self._homeTeam = data["homeTeam"] as? String
        self._overUnder = data["overUnder"] as? String
        self._location = data["location"] as? String
        self._messageId = data["messageId"] as? String
        self._matchDate = data["matchDate"] as? String
    }
    
//    init(postKey: String, dictionary: [String:AnyObject]) {
//        self._postKey = postKey
//
//        if let nameOfBar = dictionary["name"] as? String {
//            self._barName = nameOfBar
//        }
//
//        if let menuOfBar = dictionary["menu"] as? [String:AnyObject] {
//
//            for (_, value) in menuOfBar {
//                let menuItem = value["item"] as! String
//                barMenuItems.append(menuItem)
//            }
//            self._barMenu = barMenuItems
//        }
//
//        if let websiteOfBar = dictionary["website"] as? String {
//          //  let site = websiteOfBar["site"] as! String
//
//            self._barWebsite = websiteOfBar
//        }
//
//        if let menuSiteOfBar = dictionary["menuSite"] as? String {
//            //let site = menuSiteOfBar["menuSite"] as! String
//
//            self._barMenuSite = menuSiteOfBar
//        }
//
//        if let phoneNumberOfBar = dictionary["phoneNumber"] as? String {
//           // let number = phoneNumberOfBar["phoneNumber"] as! String
//
//            self._barPhoneNumber = phoneNumberOfBar
//        }
//
//        if let couponOfBar = dictionary["coupon"] as? [String:AnyObject] {
//
//            for (key, value) in couponOfBar {
//                let coupon = value["coupon"] as! String
//                barCouponsUser.append(coupon)
//                barCouponsKeyUser.append(key)
//            }
//            self._barCoupons = barCouponsUser
//            self._barCouponsKey = barCouponsKeyUser
//        }
//
//        if let coorOfBar = dictionary["coordinates"] as? [String:AnyObject] {
//            let lat = coorOfBar["lat"] as! String
//            let lon = coorOfBar["lon"] as! String
//
//            self._barLat = lat
//            self._barLon = lon
//        }
//
//        if let addressOfBar = dictionary["address"] as? String {
//            self._barAddress = addressOfBar
//        }
//
//        if let imageOfBar = dictionary["imageURL"] as? NSString {
//            self._barImage = imageOfBar
//        }
//
//        if let imagesOfBar = dictionary["userImages"] as? [String:AnyObject] {
//            for (key, value) in imagesOfBar {
//                self.barImagesKeyArray.append(key)
//                if let dict = value as? [String: AnyObject] {
//                    for (user, image) in dict {
//                        self.barImagesURLArray.append(image as? NSString)
//                        self.barImagesNameArray.append(user)
//                    }
//                }
//            }
//            self._barImagesURL = barImagesURLArray as! [NSString]
//            self._barImagesName = barImagesNameArray as! [String]
//            self._barImagesKey = barImagesKeyArray as! [String]
//        }
//
//        if let checkInsOfBar = dictionary["checkIns"] as? [String:AnyObject] {
//
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "MM-dd-yyyy H:m" /*find out and place date format from http://userguide.icu-project.org/formatparse/datetime */
//
//                for (_, value) in checkInsOfBar {
//                    if let dict = value as? [String: String] {
//                        for (user, check) in dict {
//                            //If Check is less than 2 hours append checkIns
//                            if check == "" {
//
//                            } else {
//                                let newDate = dateFormatter.date(from: check)
//                                let elapsedTime = Date().timeIntervalSince(newDate!)
//                                if elapsedTime < 7200 {
//                                    checkIn.append(check)
//                                    checkInUser.append(user)
//                                }
//                            }
//                        }
//                    }
//                }
//
//            self._barCheckIns = checkIn
//            self._barkCheckInUsers = checkInUser
//        }
//
//        if let descriptionOfBar = dictionary["desc"] as? String {
//            self._barDescription = descriptionOfBar
//        }
//
//        if let parkingOfBar = dictionary["parking"] as? [String: AnyObject] {
//
//            let uncoveredParking = parkingOfBar["uncovered"] as! String
//            let coveredParking = parkingOfBar["covered"] as! String
//            let length = parkingOfBar["length"] as! String
//
//            let uncoveredParkingNum = Int(uncoveredParking)
//            let coveredParkingNum = Int(coveredParking)
//
//            self._barParkingLength = length
//            self._barParkingUncovered = uncoveredParkingNum
//            self._barParkingCovered = coveredParkingNum
//        }
//
//        if let scheduleOfBar = dictionary["hours"] as? [String: AnyObject] {
//
//            let monday = scheduleOfBar["monday"] as! String
//            let tuesday = scheduleOfBar["tuesday"] as! String
//            let wednesday = scheduleOfBar["wednesday"] as! String
//            let thursday = scheduleOfBar["thursday"] as! String
//            let friday = scheduleOfBar["friday"] as! String
//            let saturday = scheduleOfBar["saturday"] as! String
//            let sunday = scheduleOfBar["sunday"] as! String
//
//            self._barMonday = monday
//            self._barTuesday = tuesday
//            self._barWednesday = wednesday
//            self._barThursday = thursday
//            self._barFriday = friday
//            self._barSaturday = saturday
//            self._barSunday = sunday
//        }
//
//        if let reviewOfBar = dictionary["reviews"] as? [String:AnyObject] {
//            for (_, value) in reviewOfBar {
//                let dict = value as! [String: AnyObject]
//                for (user, review) in dict {
//                    UserOfReview.append(user)
//                    let dict2 = review as! [String: String]
//                    for (star, review2) in dict2 {
//                        starsOfBar.append(Int(star)!)
//                        reviewsOfBar.append(review2)
//                    }
//                }
//            }
//            self._barReview = reviewsOfBar
//            self._barStars = starsOfBar
//            self._barReviewUser = UserOfReview
//        }
//
//        if let eventsOfBar = dictionary["events"] as? [String:AnyObject] {
//
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "MM-dd-yyyy" /*find out and place date format from http://userguide.icu-project.org/formatparse/datetime */
//
//         /*   let newDate = dateFormatter.dateFromString(check)
//            let elapsedTime = NSDate().timeIntervalSinceDate(newDate!)
//            if elapsedTime < 7200 {
//                checkIn.append(check)
//                checkInUser.append(user)*/
//           /* print("Events\(eventsOfBar)")
//            let time = eventsOfBar["time"] as! String
//            let date = eventsOfBar["date"] as! String
//            let name = eventsOfBar["name"] as! String
//            let newDate = dateFormatter.dateFromString(date)
//            let elapsedTime = NSDate().timeIntervalSinceDate(newDate!)
//            if elapsedTime < 86400 && elapsedTime >= 0 {
//                timeOfEvent.append(time)
//                dateOfEvent.append(date)
//                nameOfEvent.append(name)
//            }*/
//
//            for (key, value) in  eventsOfBar {
//                //nameOfEvent.append(key)
//                let time = value["time"] as! String
//                let date = value["date"] as! String
//                let name = value["name"] as! String
//                let newDate = dateFormatter.date(from: date)
//                let elapsedTime = Date().timeIntervalSince(newDate!)
//                if elapsedTime > -604800 && elapsedTime < 86400 { //86400 is one day
//                    timeOfEvent.append(time)
//                    dateOfEvent.append(date)
//                    nameOfEvent.append(name)
//                    keyOfEvent.append(key)
//                }
//              /*  let dict = value as! [String: String]
//                for (date, time) in dict {
//                    let newDate = dateFormatter.dateFromString(date)
//                    let elapsedTime = NSDate().timeIntervalSinceDate(newDate!)
//                    if elapsedTime < 86400 {
//
//                    }
//                    timeOfEvent.append(time)
//                    dateOfEvent.append(date)
//                } */
//            }
//            self._barEventTime = timeOfEvent
//            self._barEventName = nameOfEvent
//            self._barEventDate = dateOfEvent
//            self._barEventKey = keyOfEvent
//         //   self._barEvents = eventsOfBar
//        }
//    }
}
extension Bet: DatabaseRepresentation {
  
  var representation: [String : Any] {
    let rep: [String : Any] = [
      "created": sentDate,
      "senderUserId": sender.senderId,
      "senderUserName": sender.displayName,
      "betAmount": betAmount,
      "accepted" : false,
      "acceptedUserId": "",
      "acceptedUserName": "",
      "awayScore": "",
      "awayTeam": awayTeam,
      "homeScore": "",
      "homeTeam": homeTeam,
      "location": location,
      "overUnder": overUnder,
      "channelId": channelId,
      "status": status,
      "messageId": messageId,
      "matchDate": matchDate
    ]
    
    return rep
  }
  
}

