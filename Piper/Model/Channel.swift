//
//  Channel.swift
//  Piper
//
//  Created by Bret Rideout on 2/4/20.
//  Copyright © 2020 Rapdware. All rights reserved.
//

import Foundation
import FirebaseFirestore

struct Channel {
  
    let id: String?
    let name: String
    let memberCount: Int

    init(name: String) {
        id = nil
        self.name = name
        memberCount = 0
    }

    init?(document: DocumentSnapshot) {
        let data = document.data()

        guard let name = data!["name"] as? String else {
            return nil
        }
        id = document.documentID
        self.name = name
        if let members = data!["memberCount"] as? Int {
            memberCount = members
        } else {
            memberCount = 0
        }
    }
}

extension Channel: DatabaseRepresentation {
  
  var representation: [String : Any] {
    var rep = ["name": name]
    
    if let id = id {
      rep["id"] = id
    }
    
    return rep
  }
  
}

extension Channel: Comparable {
  
  static func == (lhs: Channel, rhs: Channel) -> Bool {
    return lhs.id == rhs.id
  }
  
  static func < (lhs: Channel, rhs: Channel) -> Bool {
    return lhs.name < rhs.name
  }

}
