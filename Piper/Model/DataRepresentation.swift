//
//  DataRepresentation.swift
//  Piper
//
//  Created by Bret Rideout on 2/4/20.
//  Copyright © 2020 Rapdware. All rights reserved.
//

import Foundation

protocol DatabaseRepresentation {
  var representation: [String: Any] { get }
}
