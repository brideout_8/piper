/// Copyright (c) 2018 Razeware LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import Firebase
import MessageKit
import FirebaseFirestore

struct Message: MessageType {

    fileprivate var _id: String?
    fileprivate var _content: String!
    fileprivate var _sentDate: Date!
    fileprivate var _sender: SenderType!
    fileprivate var _kind: MessageKind!
    fileprivate var _betAccepted: Bool!
    fileprivate var _acceptedUserId: String!
    fileprivate var _acceptedUserName: String!
    fileprivate var _betTrue: Bool!
    fileprivate var _status: String!
    fileprivate var _betAmount: String!
    fileprivate var _channelId: String!
    fileprivate var _betId: String?
    fileprivate var _homeTeam: String!
    fileprivate var _awayTeam: String!
    fileprivate var _overUnder: String!
    fileprivate var _location: String!
    fileprivate var _rejectedUsers: [String]?
    
    var id: String? {
        if let idT = _id {
            return idT
        } else {
            return nil
        }
    }
    
    var content: String {
        return _content
    }
    
    var sentDate: Date {
        return _sentDate
    }
    
    var sender: SenderType {
        return _sender
    }
    
    var kind: MessageKind {
        return _kind
    }
    
    var betAccepted: Bool {
        return _betAccepted
    }
    
    var acceptedUserId: String {
        return _acceptedUserId
    }
    
    var acceptedUserName: String {
        return _acceptedUserName
    }
    
    var betTrue: Bool {
        return _betTrue
    }
    
    var status: String {
        return _status
    }
    
    var betAmount: String {
        return _betAmount
    }
    
    var channelId: String {
        return _channelId
    }
    
    var betId: String? {
        if let betIdT = _betId {
            return betIdT
        } else {
            return nil
        }
    }
    
    var awayTeam: String {
        return _awayTeam
    }
    
    var homeTeam: String {
        return _homeTeam
    }
    
    var overUnder: String {
        return _overUnder
    }
    
    var location: String {
        return _location
    }
    
    var rejectedUsers: [String]? {
        return _rejectedUsers
    }

    
//    var senderId: String
//    var senderName: String
    
  
//  var kind: MessageKind {
//    if let image = image {
//      return .photo(image)
//    } else {
//      return .text(content)
//    }
//  }
  
  var messageId: String {
    return _id ?? UUID().uuidString
  }
  
  var image: UIImage? = nil
  var downloadURL: URL? = nil
    
//    private init(kind: MessageKind, content: String) {
//        self.kind = kind
//        sender = Sender(id: AppSettings.uid, displayName: AppSettings.displayName)
//        self.content = content
//        self.sentDate = Date()
//        id = nil
//    }
  
    init(content: String) {
        self._sender = Sender(id: AppSettings.uid, displayName: AppSettings.displayName)
        self._content = content
        self._sentDate = Date()
        self._id = nil
        self._kind = .text(content)
//        self._betAccepted = false
//        self._acceptedUserId = ""
//        self._acceptedUserName = ""
        self._betTrue = false
        self._status = KEY_BET_MESSAGE
//        self._betAmount = ""
        self._channelId = ""
//        self._betId = nil
    }
    
    init(bet: Bet, betId: String) {
        self._sender = Sender(id: AppSettings.uid, displayName: AppSettings.displayName)
        self._content = bet.betAmount
        self._sentDate = Date()
//        self._id = nil
        self._kind = .custom(_content)
        self._betTrue = true
//        self._betAmount = bet.betAmount
        self._betId = betId
        self._awayTeam = bet.awayTeam
        self._homeTeam = bet.homeTeam
        self._overUnder = bet.overUnder
        self._location = bet.location
        self._status = bet.status
        self._rejectedUsers = bet.rejectedUsers
    }
    
//    init(custom: Any?) {
//        self.sender = Sender(id: AppSettings.uid, displayName: AppSettings.displayName)
//        self.content = custom as! String
//        self.sentDate = Date()
//        self.id = nil
//        self.kind = .custom(custom)
//    }
  
//  init(user: User, image: UIImage) {
//    self.sender = Sender(id: AppSettings.uid, displayName: AppSettings.displayName)
//    self.image = image
//    self.content = ""
//    self.sentDate = Date()
//    self.id = nil
//    self.kind = .photo(image)
//    self.betAccepted = false
//    self.acceptedUserId = ""
//    self.acceptedUserName = ""
//    self.betTrue = false
//    self.senderId = sender.senderId
//    self.senderName = sender.displayName
//  }
  
    init(document: QueryDocumentSnapshot, bet: Bet? = nil) {
        let data = document.data()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
        
        if let senderID = data["senderID"] as? String {
            if let senderName = data["senderName"] as? String {
                self._sender = Sender(id: senderID, displayName: senderName)
            }
        }
        if let timeStamp = data["created"] as? Timestamp {
            let dateInt = timeStamp.dateValue()
            let dateString = dateFormatter.string(from: dateInt)
            if let sentDate = dateFormatter.date(from: dateString) {
                self._sentDate = sentDate
            }
        }
        self._id = document.documentID
//    self._status = ""
//    self._betAmount = ""
//    self._channelId = ""
//    self.senderId = sender.senderId
//    self.senderName = sender.displayName
//    switch statusServer {
//    case KEY_BET_PROPOSED:
//        //A bet
//        self._betAccepted = false
//        self._acceptedUserId = ""
//        self._acceptedUserName = ""
//        self._betTrue = true
//
//        return
//    case KEY_BET_ACCEPTED:
//        //Accepted Bet
//        return
//    case KEY_BET_REJECTED:
//        //Rejected Bet by proposer
//        return
//    default:
//        //A message
//        return
//    }
    
    if let content = data["content"] as? String {
      self._content = content
        if bet != nil {
            if let senderID = bet?.senderUserId {
                if let senderName = bet?.senderUserName {
                    self._sender = Sender(id: senderID, displayName: senderName)
                }
            }
            self._kind = .custom(content)
            self._betTrue = true
            self._betId = data["betId"] as? String
            self._betAccepted = bet!.betAccepted
            self._acceptedUserId = bet!.accectpedUserId
            self._acceptedUserName = bet!.acceptedUserName
            self._betAmount = bet!.betAmount
            self._channelId = bet!.channelId
            self._awayTeam = bet!.awayTeam
            self._homeTeam = bet!.homeTeam
            self._overUnder = bet!.overUnder
            self._location = bet!.location
            self._status = bet!.status
            self._rejectedUsers = bet!.rejectedUsers
//            self._content = bet!.status
        } else {
            self._kind = .text(content)
//            self._betAccepted = false
            self._betTrue = false
        }
      downloadURL = nil
    } else if let urlString = data["url"] as? String, let url = URL(string: urlString) {
      downloadURL = url
      _content = ""
      self._kind = .text(_content)
    } else {
    
    }
  }
}

extension Message: DatabaseRepresentation {
  
  var representation: [String : Any] {
    var rep: [String : Any] = [
      "created": sentDate,
      "senderID": sender.senderId,
      "senderName": sender.displayName,
//      "betAccepted": betAccepted,
      "bet": betTrue,
//      "acceptedUserName": acceptedUserName,
//      "acceptedUserId": acceptedUserId,
      "status": status
    ]
    
    if let url = downloadURL {
      rep["url"] = url.absoluteString
    } else {
      rep["content"] = content
    }
    if let betId = betId {
        rep["betId"] = betId
    } else {
        rep["betId"] = ""
    }
    
    return rep
  }
  
}

extension Message: Comparable {
  
  static func == (lhs: Message, rhs: Message) -> Bool {
    return lhs.id == rhs.id
  }
  
  static func < (lhs: Message, rhs: Message) -> Bool {
    return lhs.sentDate < rhs.sentDate
  }
  
}

extension UIImage: MediaItem {
  public var url: URL? { return nil }
  public var image: UIImage? { return self }
  public var placeholderImage: UIImage { return self }
  public var size: CGSize { return  CGSize.zero }
}
