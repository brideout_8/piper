//
//  DataService.swift
//  LotoSpot
//
//  Created by Bret Rideout on 5/21/16.
//  Copyright © 2016 RapidWare. All rights reserved.
//

import Foundation
import FirebaseFirestore

let URL_BASE = Firestore.firestore()
//let STORAGE_BASE = FIRStorage.storage().reference()
//var ref:FIRDatabaseReference!

class DataService {
    static let ds = DataService()
    
    fileprivate var _REF_BASE = URL_BASE
//    private var _REF_BAR_IMAGES = STORAGE_BASE.child("bar-pics")
//    private var _REF_USER_IMAGES = STORAGE_BASE.child("user-pics")
    
    var REF_BASE: Firestore {
        return _REF_BASE
    }
    
    var REF_USERS: CollectionReference {
        let users = _REF_BASE.collection("users")
        return users
    }
    
    var REF_CHANNELS: CollectionReference {
        let channels = _REF_BASE.collection("channels")
        return channels
    }
    
    func getMessages(channelId: String) -> Query {
        let messages = _REF_BASE.collection("channels")
            .document(channelId)
            .collection("thread")
            .limit(to: 5).order(by: "created", descending: true)
        return messages
    }
    
    func getMoreMessages(channelId: String, dateSent: Date) -> Query {
        let messages = _REF_BASE.collection("channels")
        .document(channelId)
        .collection("thread")
        .limit(toLast: 20)
        .order(by: "created", descending: false)
        .whereField("created", isLessThan: dateSent)
        return messages
    }
    
    func saveMessage(message: Message, channelId: String) -> [String:Any] {
        let messageSent = true
        let ref = _REF_BASE.collection("channels")
            .document(channelId)
            .collection("thread")
        let messageDocument = ref.addDocument(data: message.representation)
        return ["Message Sent":messageSent,"MessageId":messageDocument.documentID]
    }
    
    func createBet(bet: Bet) -> String {
        let betRef = _REF_BASE.collection("bets")
        let betDocument = betRef.addDocument(data: bet.representation)
//        let userRef = self._REF_BASE.collection("users").document(AppSettings.uid).collection("bets").document(betDocument.documentID)
//        userRef.setData(["status":KEY_BET_PROPOSED])
        return betDocument.documentID
    }
    
    func getBet(betId: String) -> DocumentReference {
        return _REF_BASE.collection("bets").document(betId)
    }
    
    func updateBet(betId: String, channelId: String, messageId: String, status: String) {
        let betRef = _REF_BASE.collection("bets").document(betId)
        switch status {
        case KEY_BET_WITHDRAWN:
            betRef.updateData(["status": status]) { (error) in
                if error == nil {
                    let messageRef = self._REF_BASE.collection("channels").document(channelId).collection("thread").document(messageId)
                    messageRef.updateData(["status":status])
                }
            }
        case KEY_BET_REJECTED:
            betRef.updateData(["rejectedUsers":[AppSettings.uid:AppSettings.displayName]]) { (error) in
                if error == nil {
                    let messageRef = self._REF_BASE.collection("channels").document(channelId).collection("thread").document(messageId)
                    messageRef.updateData(["rejectedUsers":[AppSettings.uid:AppSettings.displayName]])
                }
            }
        case KEY_BET_ACCEPTED:
            betRef.updateData([
                "status":KEY_BET_ACCEPTED,
                "acceptedUserId": AppSettings.uid!,
                "acceptedUserName": AppSettings.displayName!
            ]) { (error) in
                if error == nil {
                    let messageRef = self._REF_BASE.collection("channels").document(channelId).collection("thread").document(messageId)
                    messageRef.updateData(["status":KEY_BET_ACCEPTED])
                }
            }
        case KEY_BET_PROPOSED:
            betRef.updateData([
                "status": status,
                "messageId": messageId
            ])
        default:
            return
        }
    }
    
    func getUserBets(userId: String, status: String) -> Query {
        let bets = _REF_BASE.collection("bets")
            .whereField("senderUserId", isEqualTo: userId)
            .whereField("status", isEqualTo: status)
        return bets
    }
    
    func getUserAcceptedBets(userId: String) -> Query {
        let bets = _REF_BASE.collection("bets")
            .whereField("acceptedUserId", isEqualTo: userId)
            .whereField("status", isEqualTo: KEY_BET_ACCEPTED)
        return bets
    }
    
    func getChannel(channelId: String) -> DocumentReference {
        return REF_CHANNELS.document(channelId)
    }
    
    func getChannelMembers(channelId: String) -> CollectionReference {
        let users = _REF_BASE.collection("channels")
            .document(channelId)
            .collection("users")
        return users
    }
    
    func getUser(userId: String) -> DocumentReference {
        return REF_USERS.document(userId)
    }
    
    func REF_THREAD(channelId: String) -> CollectionReference{
        let thread = _REF_BASE.collection("channels").document(channelId).collection("thread")
        return thread
    }
    
    var REF_USER_CURRENT: DocumentReference {
        let uid = AppSettings.uid
        let user = _REF_BASE.collection("users").document(uid!)
        return user
    }
    
    var REF_USER_THREADS: CollectionReference {
        return REF_USER_CURRENT.collection("channels")
    }
    
//    var REF_LAND_BARS: FIRDatabaseReference {
//        let landBars = _REF_BASE.child(KEY_LAND_BARS)
//        return landBars
//    }
//
//    var REF_WATER_BARS: FIRDatabaseReference {
//        let waterBars = _REF_BASE.child(KEY_WATER_BARS)
//        return waterBars
//    }
//
//    var REF_WATER_CHECK_INS: FIRDatabaseReference {
//        let waterUid = UserDefaults.standard.value(forKey: KEY_WATER_BAR_UID) as! String
//        let checkIns = _REF_BASE.child(KEY_WATER_BARS).child(waterUid).child("checkIns")
//        return checkIns
//    }
//
//    var REF_LAND_CHECK_INS: FIRDatabaseReference {
//        let landUid = UserDefaults.standard.value(forKey: KEY_LAND_BAR_UID) as! String
//        let checkIns = _REF_BASE.child(KEY_LAND_BARS).child(landUid).child("checkIns")
//        return checkIns
//    }
//
//    var REF_LAND_BARS_REVIEWS: FIRDatabaseReference {
//        let landUid = UserDefaults.standard.value(forKey: KEY_LAND_BAR_UID) as! String
//        let landReviews = _REF_BASE.child(KEY_LAND_BARS).child(landUid).child("reviews")
//        return landReviews
//    }
//
//    var REF_WATER_BARS_REVIEWS: FIRDatabaseReference {
//        let waterUid = UserDefaults.standard.value(forKey: KEY_WATER_BAR_UID) as! String
//        let waterReviews = _REF_BASE.child(KEY_WATER_BARS).child(waterUid).child("reviews")
//        return waterReviews
//    }
//
//    var REF_CAB_TAXIS: FIRDatabaseReference {
//        let cabTaxis = _REF_BASE.child(KEY_CAB_TAXI)
//        return cabTaxis
//    }
//
//    var REF_WATER_TAXIS: FIRDatabaseReference {
//        let waterTaxis = _REF_BASE.child(KEY_WATER_TAXI)
//        return waterTaxis
//    }
//
//    var REF_BAR_IMAGES: FIRStorageReference {
//        return _REF_BAR_IMAGES
//    }
//
//    var REF_USER_IMAGES: FIRStorageReference {
//        return _REF_USER_IMAGES
//    }
//
//    var REF_LAND_BARS_PHOTOS: FIRDatabaseReference {
//        let landUid = UserDefaults.standard.value(forKey: KEY_LAND_BAR_UID) as! String
//        let landPhotos = _REF_BASE.child(KEY_LAND_BARS).child(landUid).child("userImages")
//        return landPhotos
//    }
//
//    var REF_WATER_BARS_PHOTOS: FIRDatabaseReference {
//        let waterUid = UserDefaults.standard.value(forKey: KEY_WATER_BAR_UID) as! String
//        let waterPhotos = _REF_BASE.child(KEY_WATER_BARS).child(waterUid).child("userImages")
//        return waterPhotos
//    }
    
    func createFirebaseUser(_ uid: String, user: Dictionary<String, String>) {
        REF_USERS.document(uid).setData(user)
    }
}
